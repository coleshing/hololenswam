﻿using UnityEngine;
using System.Collections;

public class SphereCommands : MonoBehaviour
{
    Vector3 originalPosition,incrementPosition;
    public Transform Target;
    private int maxIter = 20;

    // Use this for initialization
    void Start()
    {
        // Grab the original local position of the sphere when the app starts.
        originalPosition = this.transform.localPosition;
        incrementPosition = Vector3.zero;
    }

    // Called by GazeGestureManager when the user performs a Select gesture
    void OnSelect()
    {

        // Target.transform.position = this.transform.position;
        incrementPosition = (this.transform.position - Target.transform.position)/maxIter;

        StartCoroutine(effect());
        //// If the sphere has no Rigidbody component, add one to enable physics.
        //if (!this.GetComponent<Rigidbody>())
        //{
        //    var rigidbody = this.gameObject.AddComponent<Rigidbody>();
        //    rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
        //}
    }

    IEnumerator effect()
    {
        
        for ( int iteration = 0; iteration< maxIter; iteration++)
        {
            Target.transform.position = Target.transform.position + incrementPosition;
            yield return new WaitForEndOfFrame();
        }
        
        
    }
    // Called by SpeechManager when the user says the "Reset world" command
    void OnReset()
    {
        // If the sphere has a Rigidbody component, remove it to disable physics.
        var rigidbody = this.GetComponent<Rigidbody>();
        if (rigidbody != null)
        {
            DestroyImmediate(rigidbody);
        }

        // Put the sphere back into its original local position.
        this.transform.localPosition = originalPosition;
    }

    // Called by SpeechManager when the user says the "Drop sphere" command
    void OnDrop()
    {
        // Just do the same logic as a Select gesture.
        OnSelect();
    }
}