﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WamController : MonoBehaviour {


    public Transform J1Transform;
    public Transform J2Transform;
    public Transform J3Transform;
    public Transform J4Transform;
    public Transform J5Transform;
    public Transform J6Transform;
    public Transform J7Transform;
    GameObject temp;

    float[] sliders = { 0, -113, 0, 180, 0, -90, 0 }; //starting slider positions
    float[] currentAngles = { 0, -113, 0, 180, 0, -90, 0 }; //starting angles of the robot

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
       // x y z , x = rotates vertically y = rotates horizontally z = rotates facewise
        J1Transform.localRotation = Quaternion.Euler(0, -sliders[0]-90, 0); //J1 rotates around the base, neg due to physical rotation  
        J2Transform.localRotation = Quaternion.Euler(sliders[1], 90, 0); //J2 rotates vertically
        J3Transform.localRotation = Quaternion.Euler(0, -sliders[2]-180, 0); //J3 rotates around the axis of the arm
        J4Transform.localRotation = Quaternion.Euler(sliders[3]-90, -180, 0); //J4 bends the elbow
        J5Transform.localRotation = Quaternion.Euler(0, -sliders[4], 0); //J5 rotates around the wrist, negative due to physical rotation
        J6Transform.localRotation = Quaternion.Euler(sliders[5], 0, 0); //J6 bends the wrist
    }

    void OnGUI()
    {
        sliders[0] = GUI.HorizontalSlider(new Rect(25, 25, 100, 30), sliders[0], -150, 150);
        sliders[1] = GUI.HorizontalSlider(new Rect(25, 55, 100, 30), sliders[1], -113, 113);
        sliders[2] = GUI.HorizontalSlider(new Rect(25, 85, 100, 30), sliders[2], -157, 157);
        sliders[3] = GUI.HorizontalSlider(new Rect(25, 115, 100, 30), sliders[3], -50, 180);
        sliders[4] = GUI.HorizontalSlider(new Rect(25, 145, 100, 30), sliders[4], -273, 71);
        sliders[5] = GUI.HorizontalSlider(new Rect(25, 175, 100, 30), sliders[5], -90, 90);
        sliders[6] = GUI.HorizontalSlider(new Rect(25, 205, 100, 30), sliders[6], -172, 172);
    }
}
